# Express-boilerplate

## Dependency:
***

```bash
# dev
    - typescript
    - nodemon
    - tslint-config-airbnb
# prod
    - docker
    - dotenv
    - express
```

## Install
***
```bash
    - yarn install
    - create your .env file(POSTGRES_HOST, POSTGRES_PORT, POSTGRES_DB, POSTGRES_USER, POSTGRES_PASSWORD) in root project path
```
## Run for development
```bash
    - yarn dev
```
## Run for production
```bash
    WIP
```