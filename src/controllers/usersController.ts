import { Request, Response } from "express";
import Db from "src/db/users";

export const getAllusers = async (_: Request, res: Response) => {
  const data = await Db.getAllUsers();
  res.render("index", { data: JSON.stringify(data) });
};

export const addUser = async (_: Request, res: Response) => {
  await Db.addUser();
  const data = await Db.getAllUsers();
  res.render("index", { data: JSON.stringify(data) });
};
