import { Request, Response } from "express";

export const error404 = (req: Request, res: Response) => {
  res.statusCode = 404;
  res.send(`Oops 404 : ${JSON.stringify(req.body)}`);
};
