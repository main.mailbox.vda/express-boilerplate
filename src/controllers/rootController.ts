import { Request, Response } from "express";

export const rootController = (req: Request, res: Response) => {
  res.render("index", {
    data: `Opened root route with: ${JSON.stringify(req.body)}`,
  });
};
