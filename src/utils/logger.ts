export const logInfo = (type: "Info" | "Error" = "Info", data: any) => {
  console.log(`* Server_${type} *`, new Date().toISOString(), JSON.stringify(data));
};
