import fs from "fs";
import { resolve } from "path";
import { QueryFile } from "pg-promise";

export const getSqlFiles = (path: string) =>
  fs.readdirSync(path).map((file) => new QueryFile(resolve(path, file)));

export const getMapSqlFiles = (path: string): Record<string, QueryFile> => {
  let map: Record<string, QueryFile> = {};

  fs.readdirSync(path).forEach((file) => {
    const name = file.split(".sql");
    map[name[0]] = new QueryFile(resolve(path, file));
  });

  return map;
};
