import { resolve } from "path";
import { getMapSqlFiles, getSqlFiles } from "src/utils/query";

export const queriesArr = {
  tablesQuery: getSqlFiles(resolve(__dirname, "tables")),
};

export default {
  usersQuery: getMapSqlFiles(resolve(__dirname, "users")),
};
