import pgp from "pg-promise";

export type TInitialOptions = Parameters<pgp.IMain>[0];

const initialOptions: TInitialOptions = {
  host: process.env.POSTGRES_HOST || "localhost",
  port: process.env.POSTGRES_PORT ? parseInt(process.env.POSTGRES_PORT) : 5432,
  database: process.env.POSTGRES_DB || "db",
  user: process.env.POSTGRES_USER || "postgres",
  password: process.env.POSTGRES_PASSWORD || "password",
};

export default initialOptions;
