import pgp from "pg-promise";
import { TDbInstance, TDbQuery, TDbQueryObjects } from "src/types/db.types";
import { logInfo } from "src/utils/logger";

import Queries, { queriesArr } from "./sql";
import initialDbOptions from "./initialDbOptions";

class PostgresDB {
  private db: TDbInstance;

  constructor() {
    this.db = pgp()(initialDbOptions);
  }

  public initial = () => {
    this.initialTables();
  };

  public query = async (
    querySet: TDbQueryObjects,
    queryName: keyof TDbQuery
  ) => {
    const calledQuery = Queries[querySet][queryName];

    if (calledQuery) {
      try {
        const result = await this.db.any<TDbQuery[keyof TDbQuery]>(calledQuery);
        logInfo("Info", `Call ${queryName}: success`);
        return result;
      } catch (error: unknown) {
        logInfo("Error", error);
      }
    }
    logInfo("Error", `Unknown query: ${queryName}`);
    return null;
  };

  private initialTables = () => {
    queriesArr.tablesQuery.forEach(async (query) => {
      try {
        await this.db.none(query);
      } catch (error: unknown) {
        logInfo("Error", error);
      }
    });
  };
}

const db = new PostgresDB();

export default db;
