import Db from "../Db";

class Users {
  getAllUsers = () => Db.query("usersQuery", "getAllUsers");

  addUser = () => Db.query("usersQuery", "addUser");

  updateUser = () => {};

  deleteUser = () => {};
}

const users = new Users();

export default users;
