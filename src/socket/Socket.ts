import http from "http";
import { Server } from "socket.io";
import { logInfo } from "src/utils/logger";

class Socket {
  io: Server | null = null;

  initital(httpServer: http.Server) {
    this.io = new Server(httpServer, {
      // TODO:
      cors: { origin: "*", methods: ["GET", "POST"] },
    });

    this.listenConnections();
  }

  listenConnections = () => {
    const { io } = this;

    if (io) {
      io.on("connection", (socket) => {
        logInfo("Info", `User connected: ${socket.id}`);
      });
    }
  };
}

const socket = new Socket();

export default socket;
