import { Request, Response, NextFunction } from "express";
import { logInfo } from "src/utils/logger";

export default (req: Request, _: Response, next: NextFunction) => {
  logInfo("Info", {
    method: req.method,
    originalUrl: req.originalUrl,
    params: req.method === "GET" ? req.query : req.body,
  });

  next();
};
