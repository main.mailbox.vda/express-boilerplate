import express from "express";
import { createServer } from "http";
import cors from "cors";
import path from "path";
import dotenv from "dotenv";
import { logInfo } from "src/utils/logger";
import requestLogger from "src/middleware/requestLogger";
import db from "src/db";
import socket from "src/socket";
import { initialRoutes } from "src/routes";

const app = express();
const httpServer = createServer(app);

/**
 * ENV
 */
dotenv.config();
const port = process.env.PORT;

/**
 * Define folder for static views(ejs used)
 */
app.set("view engine", "ejs");
app.set("views", path.resolve(__dirname, "static"));

/**
 * Middlewares
 */
app.use(requestLogger);
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

/**
 * Initial DB
 */
db.initial();

/**
 * Routes
 */
initialRoutes(app);

/**
 * Initial Socket
 */
socket.initital(httpServer);

/**
 * Server info
 */
httpServer.listen(port, () => {
  logInfo("Info", `server is listening on  ${process.env.HOST}:${port}`);
});
