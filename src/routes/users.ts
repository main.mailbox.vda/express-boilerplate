import express from "express";
import { getAllusers, addUser } from "src/controllers/usersController";

import routesMap from "./routesMap";

const router = express.Router();

router.get(routesMap.users.all, getAllusers);

router.get(routesMap.users.add, addUser);

export default router;
