import express from "express";
import { rootController } from "src/controllers/rootController";

import routesMap from "./routesMap";

const router = express.Router();

router.get(routesMap.root, rootController);

router.post(routesMap.root, rootController);

export default router;
