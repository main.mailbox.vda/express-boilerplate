import { Express } from "express";

import root from "./root";
import users from "./users";
import notFound from "./notFound";
import routesMap from "./routesMap";

export const initialRoutes = (app: Express) => {
  app.use(routesMap.root, root);
  app.use(routesMap.users.root, users);
  app.use(routesMap.unknown, notFound);
};
