export default {
  root: "/",
  users: { root: "/users", all: "/all", add: "/add" },
  unknown: "*",
};
