import express from "express";

import routesMap from "./routesMap";

const router = express.Router();

router.get(routesMap.unknown, (req, res) => {
  res.statusCode = 404;
  res.send(`Oops 404: ${JSON.stringify(req.query)}`);
});

router.post(routesMap.unknown, (req, res) => {
  res.statusCode = 404;
  res.send(`Oops 404 : ${JSON.stringify(req.body)}`);
});

export default router;
