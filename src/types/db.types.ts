import { IDatabase, QueryFile } from "pg-promise";

import { TUserQueries, TUserQueriesId } from "./db/users.types";

export type TDbInstance = IDatabase<Record<string, unknown>>;

export type TObjQuery = Record<string, QueryFile>;

export type TDbQueryObjects = TUserQueriesId;

export type TDbQuery = TUserQueries;
