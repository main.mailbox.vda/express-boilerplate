export type TUser = {
  email: string;
  verify: boolean;
  region: string;
};

// TODO:
export type TAddUserResponse = null;

// TODO:
export type TGetAllUsersResponse = Array<TUser>;

export type TUserQueries = {
  addUser: TAddUserResponse;
  getAllUsers: TGetAllUsersResponse;
};

export type TUserQueriesId = "usersQuery";
